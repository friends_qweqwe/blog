package com.awesome.blog.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class MockController {

    @GetMapping("/echo/{text}")
    public String echo(@PathVariable String text) {
        return text;
    }
}