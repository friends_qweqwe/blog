package com.awesome.blog.service.impl;

import com.awesome.blog.dao.UserRepository;
import com.awesome.blog.dto.UserDto;
import com.awesome.blog.mapper.UserMapper;
import com.awesome.blog.model.User;
import com.awesome.blog.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Override
    @Transactional
    public void save(UserDto userDto) {
        User user = userMapper.mapToEntity(userDto);
        userRepository.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDto getUserById(Long id) {
        Optional<User> user = userRepository.findById(id);
        return user
                .map(userMapper::mapToDto)
                .orElseThrow(() -> new NoSuchElementException("User can not be received. Invalid user id!"));
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserDto> getAllUsers() {
        return userMapper.mapToList(userRepository.findAll());
    }

    @Override
    @Transactional
    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }
}