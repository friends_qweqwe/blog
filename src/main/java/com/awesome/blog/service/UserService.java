package com.awesome.blog.service;

import com.awesome.blog.dto.UserDto;

import java.util.List;

public interface UserService {

    void save(UserDto userDto);

    UserDto getUserById(Long id);

    List<UserDto> getAllUsers();

    void deleteUserById(Long id);
}