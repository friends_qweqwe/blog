# Blog Application
## Startup
In order to build this project clone repository, open `Blog` folder and execute
`./gradlew clean build` on `linux` or `gradlew.bat clean build` on `windows` machine.

After this you may run application with the following command: `java -jar build/libs/blog-0.0.1-SNAPSHOT.jar` 
on `linux` or `java -jar build\libs\blog-0.0.1-SNAPSHOT.jar` on `windows` machine. 